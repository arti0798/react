import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';

class App extends Component {
    render() { //react call this render method to render something on screen (we always have to render some HTML code to DOM)
        // return ( <
        //     div className = "App" >
        //     <
        //     h1 > Hi i m React App < /h1>  < /
        //     div >
        // );

        // return React.createElement('div', null, 'h1', 'Hi, I\'m a React App')

        return React.createElement('div', null, React.createElement('h1', { className: 'App' }, 'DOes this work now?'));
    }
}

export default App;